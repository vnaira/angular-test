import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {CategoryComponent} from './category/category.component';
import {ProductComponent} from './product/product.component';
import {BasketService} from "./services/basket.service";
import {BasketComponent} from './basket/basket.component';
import {ProductdetailsComponent} from './productdetails/productdetails.component';


const MainRoute: Routes = [
  {
    path: '',
    component: CategoryComponent,
    children: [
      {
        path: 'product',
        component: ProductComponent,
        children: [{
          path: 'details',
          component: ProductdetailsComponent,
        }]
      }
    ]
  }];


@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    ProductComponent,
    BasketComponent,
    ProductdetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(MainRoute)
  ],
  providers: [BasketService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
