import { Component } from '@angular/core';

import {Router, ActivatedRoute, Params} from '@angular/router';
import { BasketService } from './services/basket.service';
import {CategoryComponent} from './category/category.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'app works!';
  
}
