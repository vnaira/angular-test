import {Component, OnInit, Input} from '@angular/core';
import {Router, ActivatedRoute, Params} from "@angular/router";
import {BasketService} from "../services/basket.service";
import {Product} from "../product/product";

@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.css']
})
export class ProductdetailsComponent implements OnInit {
  
  @Input() details: any = {};
  
  public productCount:number;
  
  productDetail;
  prodList;
  
  constructor(private activatedRoute: ActivatedRoute,
              private dataService: BasketService) {
  }
  
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.productDetail = this.dataService.categorys[params['categoryId']] || {};
      
      this.details = this.productDetail.productList[params['index']];
    });
  }
  
  
  addToBasket(name: string, price: number, count: number): void {
    this.prodList = this.dataService.basket;
    if (name == null || name == undefined)
      return;
    if (price == null || price == undefined)
      return;
    this.prodList.push(new Product(name, price));
    
    this.productCount = this.prodList.length;
  }
  
  removeFromBasket(prod) {
    let index = this.prodList.indexOf(prod);
    
    if (index !== -1) {
      this.prodList.splice(index, 1);
    }
  }
  
  
}
