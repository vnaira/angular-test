import {Component} from "@angular/core";

@Component({})
export class Product {
  purchase: string;
  price: number;
  
  constructor(purchase: string, price: number) {
    this.purchase = purchase;
    this.price = price;
  }
}
