import {Component, OnInit, enableProdMode, Input} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Product} from "./product";
import {BasketService} from "../services/basket.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  prodList;
  product;
  
  //@Input()  details: any = {};
  
  constructor(private activatedRoute: ActivatedRoute,
              private dataService: BasketService, private router: Router) {
  }
  
  
  ngOnInit() {
    //this.activatedRoute.params.subscribe((params: Params) => {
    //  this.productId = params['id'];
    //})
    
    
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.product = this.dataService.categorys[params['index']];
    });
    
  }
  
  //
  //addToBasket(name: string, price: number, count: number): void {
  //  this.prodList = this.dataService.basket;
  //  if (name == null || name == undefined)
  //    return;
  //  if (price == null || price == undefined)
  //    return;
  //  this.prodList.push(new Product(name, price));
  //
  //  this.dataService.productCount = this.prodList.length;
  //
  //}
  
  
  
}
