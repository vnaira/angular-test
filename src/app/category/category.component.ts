import {Component, OnInit} from '@angular/core';
import {BasketService} from '../services/basket.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  
  categorylist;
  
  
  constructor(private basketService: BasketService,
              private router: Router) {
  }
  
  ngOnInit() {
    this.categorylist = this.basketService.categorys;
  }
  
  navigateToProduct(i: number, cat_id:number) {
    this.router.navigate(['/product/details'], {
      queryParams: {
        index:i,
        categoryId:cat_id
      }
    });
  }
}
