import {Injectable} from "@angular/core";
import {Product} from "../product/product";

@Injectable()
export class BasketService {
  
  basket: Product[] = [];
  productCount:number = this.basket.length;

  
  items: Product[] =
    [
      {purchase: "Product1", price: 15.9},
      {purchase: "Product2", price: 60},
      {purchase: "Product3",  price: 22.6},
      {purchase: "Product4", price: 310}
    ];
  
  constructor(){}
  
  categorys = [{
    label: "Category 1",
    id: 0,
    productList: this.items
  }, {
    label: "Category 2",
    id: 1,
    productList: this.items
  }, {
    label: "Category 3",
    id: 2,
    productList: this.items
  }, {
    label: "Category 4",
    id: 3,
    productList: this.items
  }, {
    label: "Category 5",
    id: 4,
    productList: this.items
  }];
  
}
